package fanketly.core.lib.router.module.login

import com.alibaba.android.arouter.facade.Postcard
import fanketly.core.lib.router.module.home.HomeRouter

object LoginNav {

    // ==========
    // = 快捷方法 =
    // ==========

    /**
     * 内部传入 GROUP 尽量各个模块直接通过对应 [build] 方法跳转
     * 便于代码跳转直观、对外避免跳转错 GROUP ( Module )
     */
    fun build(path: String): Postcard {
        return LoginRouter.build(path)
    }

    // ==========
    // = 跳转方法 =
    // ==========

    /**
     * 模块入口路由跳转
     */
    fun routerMain() {
        build(LoginRouter.PATH_MAIN).navigation()
    }

}