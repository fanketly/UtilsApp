package com.fanketly.lib_dialog

import android.app.AlertDialog
import android.content.Context
import android.view.View


/**
 *Create by Fanketly on 2022/7/25
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
object DialogUtils {
    fun newDialog(
        context: Context,
        title: String,
        view: View,
        negative: (() -> Unit)? = null,
        positive: () -> Unit
    ) {
        AlertDialog.Builder(context)
            .setTitle(title)
            .setView(view)
//            .setNegativeButton("取消") { dialog, _ ->
//                negative?.invoke()
//                dialog.dismiss()
//            }
//            .setPositiveButton("确定") { dialog, _ ->
//                positive.invoke()
//                dialog.dismiss()
//            }
            .create()
            .show()
    }
}