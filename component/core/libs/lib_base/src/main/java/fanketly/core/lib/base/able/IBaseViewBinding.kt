package fanketly.core.lib.base.able

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding

/**
 *Create by Fanketly on 2022/7/1
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
interface IBaseViewBinding<VB : ViewBinding> {

    /**
     * View Bind 初始化操作
     * @param inflater  [LayoutInflater]
     * @param container [ViewGroup]
     */
    fun viewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        isAttach: Boolean
    ): VB


    /**
     * 是否 Bind View
     */
    fun isAddBarView(): Boolean {
        return false
    }

    /**
     * 是否分离 ( 销毁 ) Binding
     */
    fun isDetachBinding(): Boolean {
        return false
    }
}