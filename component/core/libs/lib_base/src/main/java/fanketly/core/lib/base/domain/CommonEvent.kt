package fanketly.core.lib.base.domain

/**
 *Create by Fanketly on 2022/9/15
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
sealed class CommonEvent {
    data class GetList(var list: List<*>? = null) : CommonEvent()
    data class Add(var any: Any? = null) : CommonEvent()
    data class Switch(var sourceModel: Any? = null, var isSuccess: Boolean = true) : CommonEvent()
    data class Delete(var isSuccess: Boolean = true) : CommonEvent()
    data class Edit(var isSuccess: Boolean = true) : CommonEvent()
    data class QuerySome(var map: Map<String, *>? = null, var list: List<*>? = null) : CommonEvent()
    data class QueryAny(var map: Map<String, *>? = null, var sourceModel: Any? = null) :
        CommonEvent()
}