package fanketly.core.lib.base.ui

import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.blankj.utilcode.util.LogUtils
import fanketly.core.lib.base.able.IBaseViewModel
import fanketly.core.lib.base.utils.ClassUtils
import fanketly.core.lib.base.utils.assist.BaseViewModelAssist

/**
 *Create by Fanketly on 2022/7/1
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
abstract class BaseVMActivity<VB : ViewBinding, VM : ViewModel> : BaseViewBindingActivity<VB>(),
    IBaseViewModel<VM> {


    @JvmField // DevBase ViewModel 辅助类
    protected var mViewModelAssist = BaseViewModelAssist()

    lateinit var mViewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }


    override fun initViewModel() {
        try {
            val clazz = ClassUtils.getGenericSuperclass(this.javaClass, 1) as Class<VM>
            mViewModelAssist.getActivityViewModel(
                this, clazz
            )?.let {
                mViewModel = it
            }
        } catch (e: Exception) {
            LogUtils.e(e)
        }
    }


    override fun <T : ViewModel> getActivityViewModel(modelClass: Class<T>): T? {
        TODO("Not yet implemented")
    }

    override fun <T : ViewModel> getFragmentViewModel(modelClass: Class<T>): T? {
        TODO("Not yet implemented")
    }

    override fun <T : ViewModel> getFragmentViewModel(
        fragment: Fragment?,
        modelClass: Class<T>
    ): T? {
        TODO("Not yet implemented")
    }

    override fun <T : ViewModel> getAppViewModel(
        application: Application?,
        modelClass: Class<T>
    ): T? {
        TODO("Not yet implemented")
    }

    override fun getAppViewModelProvider(application: Application?): ViewModelProvider? {
        TODO("Not yet implemented")
    }

    override fun getAppFactory(application: Application?): ViewModelProvider.Factory? {
        TODO("Not yet implemented")
    }
}