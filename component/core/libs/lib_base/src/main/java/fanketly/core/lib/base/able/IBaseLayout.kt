package fanketly.core.lib.base.able

import android.view.View

/**
 *Create by Fanketly on 2022/7/1
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
interface IDevBaseLayout {

    /**
     * 获取 Layout Id ( 用于 contentLinear addView )
     * @return Layout Id
     */
    fun baseLayoutId(): Int

    /**
     * 获取 Layout View ( 用于 contentLinear addView )
     * @return Layout View
     */
    fun baseLayoutView(): View?

    /**
     * Layout View addView 是否 LayoutParams.MATCH_PARENT
     */
    fun isLayoutMatchParent(): Boolean {
        return true
    }
}