package fanketly.core.lib.base.utils

import android.util.Log
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 *Create by Fanketly on 2022/7/3
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
object ClassUtils {
    /**
     * 获取父类泛型类型
     * @param clazz [Class]
     * @param pos   泛型参数索引
     * @return 泛型类型
     */
    fun getGenericSuperclass(
        clazz: Class<*>?,
        pos: Int
    ): Type? {
        if (clazz != null && pos >= 0) {
            try {
                return (clazz.genericSuperclass as ParameterizedType)
                    .actualTypeArguments[pos]
            } catch (e: Exception) {
                Log.e("getGenericSuperclass", "getGenericSuperclass: $e")
            }
        }
        return null
    }


}