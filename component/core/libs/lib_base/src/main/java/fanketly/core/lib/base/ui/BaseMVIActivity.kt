package fanketly.core.lib.base.ui

import android.os.Bundle
import androidx.lifecycle.ViewModel
import androidx.viewbinding.ViewBinding

/**
 *Create by Fanketly on 2022/8/18
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
abstract class BaseMVIActivity<VB : ViewBinding, VM : ViewModel> : BaseVMActivity<VB, VM>() {
    abstract fun initView()
    abstract fun initData()
    abstract fun onInput()
    abstract fun onOutput()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initData()
        onOutput()
        onInput()
    }

}