package fanketly.core.lib.base.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.alibaba.android.arouter.launcher.ARouter
import com.blankj.utilcode.util.LogUtils
import fanketly.core.lib.base.able.IBaseController
import fanketly.core.lib.base.able.IBaseViewBinding
import fanketly.core.lib.base.utils.ViewBindingUtils

/**
 *Create by Fanketly on 2022/7/1
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
abstract class BaseViewBindingActivity<VB : ViewBinding> : AppCompatActivity(),
    IBaseController, IBaseViewBinding<VB> {

    private var _binding: VB? = null
    val binding: VB get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (isAddARouterInject()) {
            try {
                ARouter.getInstance().inject(this)
            } catch (e: Exception) {
                LogUtils.i(e)
            }
        }
        // ViewBinding 初始化处理
        _binding = if (isAddBarView()) {
            viewBinding(layoutInflater, null, true)
        } else {
            viewBinding(layoutInflater, null, false)
        }
        setContentView(_binding?.root)
    }

    final override fun viewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        isAttach: Boolean
    ): VB {
        return ViewBindingUtils.viewBindingJavaClass(
            inflater,
            container,
            isAttach,
            javaClass
        )
    }


}