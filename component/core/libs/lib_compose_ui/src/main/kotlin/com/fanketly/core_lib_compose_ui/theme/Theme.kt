package com.fanketly.core_lib_compose_ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import com.fanketly.core_lib_compose_ui.theme.*

private val DarkColorPalette = darkColors(
    primary = Purple200,
    primaryVariant = Green200,
    secondary = Green200
)

private val LightColorPalette = lightColors(
    primary = Green200,
    primaryVariant = Green200,
    secondary = Green300,
    secondaryVariant = Green400,
    onSecondary = Alpha70White,
    onBackground = Green300
    /* Other default colors to override
    background = Color.White,
    surface = Color.White,
    onPrimary = Color.White,
    onSecondary = Color.Black,
    onBackground = Color.Black,
    onSurface = Color.Black,
    */
)

@Composable
fun UtilsAppTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}