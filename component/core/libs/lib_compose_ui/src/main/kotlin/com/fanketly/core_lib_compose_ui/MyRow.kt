package com.fanketly.core_lib_compose_ui

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.sp

/**
 *Create by Fanketly on 2022/9/19
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
@Composable
fun IconRowItem(
    painter: Painter,
    contentDescription: String? = null,
    textId: Int,
    rowModifier: Modifier = Modifier,
    iconModifier: Modifier = Modifier,
    textModifier: Modifier = Modifier
) {
    Row(verticalAlignment = Alignment.CenterVertically, modifier = rowModifier.fillMaxWidth()) {
        Icon(painter = painter, contentDescription = contentDescription, modifier = iconModifier)
        Text(text = stringResource(id = textId), modifier = textModifier, fontSize = 16.sp)
    }
}