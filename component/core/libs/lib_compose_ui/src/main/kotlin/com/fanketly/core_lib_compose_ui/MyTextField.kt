package com.fanketly.core_lib_compose_ui

import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import com.fanketly.core_lib_compose_ui.theme.Green200
import com.fanketly.core_lib_compose_ui.theme.Green300

/**
 *Create by Fanketly on 2022/9/15
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
@Composable
fun UserNameTextField(textValue: MutableState<String>, modifier: Modifier = Modifier) {
    CommonTextField(textValue = textValue, modifier = modifier, label = {
        Text(text = stringResource(id = R.string.core_ui_username))
    })
}

@Composable
fun PasswordTextField(textValue: MutableState<String>, modifier: Modifier = Modifier) {
    CommonTextField(
        textValue = textValue,
        keyboardType = KeyboardType.Password,
        modifier = modifier,
        label = {
            Text(text = stringResource(id = R.string.core_ui_password))
        })
}

@Composable
fun CommonTextField(
    modifier: Modifier = Modifier,
    textValue: MutableState<String>,
    unfocusedColor: Color = Green200,
    focusedColor: Color = Green300,
    keyboardType: KeyboardType = KeyboardType.Text,
    label: @Composable (() -> Unit)? = null
) {
    OutlinedTextField(
        value = textValue.value,
        onValueChange = {
            textValue.value = it
        },
        label = label,
        modifier = modifier,
        colors = TextFieldDefaults.outlinedTextFieldColors(
            unfocusedBorderColor = unfocusedColor,
            unfocusedLabelColor = unfocusedColor,
            focusedBorderColor = focusedColor,
            focusedLabelColor = focusedColor,
            cursorColor = focusedColor,
        ),
//keyboardOptions：设置KeyboardType、ImeAction等键盘选项。一些可用的键盘类型是：电子邮件、密码和数字，
// 而重要的 ImeAction 是：Go、Search、Previous、Next 和 Done。
        keyboardOptions = KeyboardOptions.Default.copy(keyboardType = keyboardType)
    )
}