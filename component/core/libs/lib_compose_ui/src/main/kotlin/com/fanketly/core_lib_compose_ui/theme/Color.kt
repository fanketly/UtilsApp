package com.fanketly.core_lib_compose_ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)

val Teal200 = Color(0xFF03DAC5)

val Green100 = Color(0xFFB2DFDB)

val Green200 = Color(0xFF80CBC4)
val Green300 = Color(0xFF4DB6AC)
val Green400 = Color(0xFF26A69A)

val Alpha70White = Color(0xB3FFFFFF)
val Alpha70Grey = Color(0xB3C9C8C8)

