package com.fanketly.core_lib_compose_ui

import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.sp

@Composable
fun TipDialogBooleanState(
    tipContent: String,
    openDialog: MutableState<Boolean>,
    onConfirm: () -> Unit
) {
    CommonDialog(title = "提示", text = {
        Text(text = tipContent)
    }, onDismissRequest = { openDialog.value = false }, onDismissClick = {
        openDialog.value = false
    }) {
        onConfirm.invoke()
        openDialog.value = false
    }
}

@Composable
fun <E> TipDialogModelState(
    tipContent: String,
    openDialog: MutableState<E?>,
    onConfirm: (model: E) -> Unit
) {
    if (openDialog.value != null)
        CommonDialog(title = "提示", text = {
            Text(text = tipContent)
        }, onDismissRequest = { openDialog.value = null }, onDismissClick = {
            openDialog.value = null
        }) {
            onConfirm.invoke(openDialog.value!!)
            openDialog.value = null
        }
}

@Composable
fun <E> TextFiledDialogModelState(
    title: String,
    openDialog: MutableState<E?>,
    valueState: MutableState<String> = mutableStateOf(""),
    onConfirm: (String) -> Unit
) {
    if (openDialog.value != null)
        CommonDialog(title = title, text = {
            TextField(
                value = valueState.value,
                onValueChange = { valueState.value = it },
                colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White),
            )
        }, onDismissRequest = { openDialog.value = null }, onDismissClick = {
            openDialog.value = null
        }) {
            onConfirm.invoke(valueState.value)
            openDialog.value = null
        }
}

@Composable
fun TextFiledDialog(
    title: String,
    openDialog: MutableState<Boolean>,
    valueState: MutableState<String> = mutableStateOf(""),
    onConfirm: (String) -> Unit
) {
    CommonDialog(title = title, text = {
        TextField(
            value = valueState.value,
            onValueChange = { valueState.value = it },
            colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White),
        )
    }, onDismissRequest = { openDialog.value = false }, onDismissClick = {
        openDialog.value = false
    }) {
        onConfirm.invoke(valueState.value)
        openDialog.value = false
    }
//    AlertDialog(
//        onDismissRequest = { openDialog.value = false },
//        confirmButton = {
//            Button(
//                onClick = {
//                    onConfirm.invoke(valueState.value)
//                    openDialog.value = false
//                }, modifier = Modifier
//            ) {
//                Text(text = "确定", fontSize = 14.sp)
//            }
//        },
//        dismissButton = {
//            Button(
//                onClick = {
//                    openDialog.value = false
//                }, modifier = Modifier
//            ) {
//                Text(text = "取消", fontSize = 14.sp)
//            }
//        },
//        title = {
//            Text(text = title, fontSize = 16.sp)
//        },
//        text = {
//            TextField(
//                value = valueState.value,
//                onValueChange = { valueState.value = it },
//                colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White),
//            )
//        })
}

@Composable
fun CommonDialog(
    title: String,
    text: @Composable (() -> Unit),
    onDismissRequest: () -> Unit,
    onDismissClick: () -> Unit,
    onConfirmClick: () -> Unit
) {
    AlertDialog(
        onDismissRequest = onDismissRequest,
        confirmButton = {
            Button(
                onClick = onConfirmClick, modifier = Modifier
            ) {
                Text(text = "确定", fontSize = 14.sp)
            }
        },
        dismissButton = {
            Button(
                onClick = onDismissClick, modifier = Modifier
            ) {
                Text(text = "取消", fontSize = 14.sp)
            }
        },
        title = {
            Text(text = title, fontSize = 16.sp)
        },
        text = text
    )
}