package com.fanketly.core_lib_compose_ui

import androidx.compose.foundation.layout.RowScope
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

/**
 *Create by Fanketly on 2022/9/19
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
@Composable
fun MyTopAppBar(scaffoldState: ScaffoldState, scope: CoroutineScope) {
    val drawerState = scaffoldState.drawerState
    TopAppBar(
        navigationIcon = {
            IconButton(
                content = {
                    Icon(
                        Icons.Default.Menu,
                        tint = Color.White,
                        contentDescription = "menu"
                    )
                },
                onClick = {
                    scope.launch {
                        if (drawerState.isClosed)
                            drawerState.open() else drawerState.close()
                    }
                }
            )
        },
        title = {
            Text(text = "工具箱", color = Color.White)
        },
        backgroundColor = MaterialTheme.colors.primary
    )
}

@Composable
fun MyBackTopAppBar(text: String,actions: @Composable RowScope.() -> Unit = {}, onClick: () -> Unit) {
    TopAppBar(
        navigationIcon = {
            IconButton(
                content = {
                    Icon(
                        Icons.Default.ArrowBack,
                        tint = Color.White,
                        contentDescription = "back"
                    )
                },
                onClick = onClick
            )
        },
        title = {
            Text(text = text, color = Color.White)
        },
        actions = actions,
        backgroundColor = MaterialTheme.colors.primary
    )
}

@Composable
fun MyBottomAppBar() {

}
