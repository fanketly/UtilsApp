package com.fanketly.core_lib_compose_ui.data

/**
 *Create by Fanketly on 2022/9/19
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
data class DrawerItem(
    var painterId: Int,
    var contentDescription: String? = null,
    var textId: Int
)
