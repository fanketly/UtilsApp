package fanketly.core.extension

/**
 *Create by Fanketly on 2022/8/18
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
fun <E> MutableList<E>.addOnBRV(e: E): Int {
    add(e)
    return size
}