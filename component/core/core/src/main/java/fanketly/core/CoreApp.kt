package fanketly.core

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context

/**
 *Create by Fanketly on 2022/7/16
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
abstract class CoreApp : Application() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        private var _context: Context? = null
        val context: Context
            get() = _context!!
    }

    override fun onCreate() {
        super.onCreate()
        _context = applicationContext
    }
}