package fanketly.core.utils

import ando.file.core.FileGlobal
import ando.file.core.FileUtils
import ando.file.selector.*
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.activity.result.ActivityResultLauncher
import com.fanketly.core_lib_bean.REQUEST_CHOOSE_FILE

fun selectPhoto(
    context: Context,
    launcher: ActivityResultLauncher<Intent>,
    callBack: FileSelectCallBack
): FileSelector {
    val optionsImage = FileSelectOptions().apply {
        fileType = FileType.IMAGE
        fileTypeMismatchTip = "文件类型不匹配 !" //File type mismatch
        singleFileMaxSize = 5242880
        singleFileMaxSizeTip = "图片最大不超过5M !" //The largest picture does not exceed 5M
        allFilesMaxSize = 10485760
        allFilesMaxSizeTip =
            "总图片大小不超过10M !"//The total picture size does not exceed 10M  注:单选条件下无效,只做单个图片大小判断
        fileCondition = object : FileSelectCondition {
            override fun accept(fileType: IFileType, uri: Uri?): Boolean {
                return (fileType == FileType.IMAGE && uri != null && !uri.path.isNullOrBlank() && !FileUtils.isGif(
                    uri
                ))
            }
        }
    }
    return FileSelector
        .with(context, launcher)
        .setRequestCode(REQUEST_CHOOSE_FILE)
        .setTypeMismatchTip("文件类型不匹配 !") //File type mismatch
        .setMinCount(1, "至少选择一个文件 !") //Choose at least one file
        .setMaxCount(10, "最多选择十个文件 !") //Choose up to ten files  注:单选条件下无效, 只做最少数量判断
        .setOverLimitStrategy(FileGlobal.OVER_LIMIT_EXCEPT_OVERFLOW)
        .setSingleFileMaxSize(
            1048576,
            "大小不能超过1M !"
        ) //The size cannot exceed 1M  注:单选条件下无效, FileSelectOptions.singleFileMaxSize
        .setAllFilesMaxSize(
            10485760,
            "总大小不能超过10M !"
        ) //The total size cannot exceed 10M 注:单选条件下无效,只做单个图片大小判断 setSingleFileMaxSize
        .setExtraMimeTypes("image/*") //默认不做文件类型约束为"*/*",不同类型系统提供的选择UI不一样 eg:"video/*","audio/*","image/*"
        .applyOptions(optionsImage)
        .filter(object : FileSelectCondition {
            override fun accept(fileType: IFileType, uri: Uri?): Boolean {
                return when (fileType) {
                    FileType.IMAGE -> (uri != null && !uri.path.isNullOrBlank() && !FileUtils.isGif(
                        uri
                    ))
                    FileType.VIDEO -> false
                    FileType.AUDIO -> false
                    else -> false
                }
            }
        })
        .callback(callBack)
        .choose()
}
