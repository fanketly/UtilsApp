package com.fanketly.utilsapp

import ando.file.core.FileOperator
import com.alibaba.android.arouter.launcher.ARouter
import com.drake.brv.utils.BRV
import fanketly.core.CoreApp


/**
 *Create by Fanketly on 2022/6/26
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
class BaseApplication : CoreApp() {

    override fun onCreate() {
        super.onCreate()
        ARouter.init(this)
        BRV.modelId = BR.m
        FileOperator.init(this, BuildConfig.DEBUG)
//        SafeNav.routerMain()
    }

}