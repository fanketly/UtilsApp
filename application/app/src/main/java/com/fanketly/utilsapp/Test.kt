package com.fanketly.utilsapp

import androidx.compose.foundation.clickable
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.fanketly.core_lib_compose_ui.theme.UtilsAppTheme
import com.fanketly.module_safe_photo.data.model.SafePhotoSortModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 *Create by Fanketly on 2022/9/14
 *Gitee:https://gitee.com/fanketly
 *desc:
 */

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    val state = mutableStateListOf(
        SafePhotoSortModel("AA", "", 1, 1),
        SafePhotoSortModel("AA", "", 1, 1),
        SafePhotoSortModel("AA", "", 1, 1),
        SafePhotoSortModel("AA", "", 1, 1),
    )
    UtilsAppTheme {
//        TestFiledDialog()
        TestLazyColumn(list = state)
    }
    CoroutineScope(Dispatchers.IO).launch {
        delay(3000L)
        state.forEach { it.sortName = "BB" }
    }
}

@Composable
fun TestLazyColumn(list: SnapshotStateList<SafePhotoSortModel>) {
    LazyColumn {
        items(list) {
            TestText(text = it.sortName, modifier = Modifier.clickable {
//                list.add(SafePhotoSortModel("AA", "", 1, 1))
//                it.sortName = "BB"
//                list[0] = it
            })
        }
    }
}

@Composable
fun TestText(text: String, modifier: Modifier) {
    println(text)
    Text(text = text, modifier = modifier)
}

@Composable
fun TestFiledDialog() {
    val valueState = remember {
        mutableStateOf("")
    }
    AlertDialog(
        onDismissRequest = { },
        confirmButton = {
            Button(
                onClick = {
                }, modifier = Modifier
//                    .height(55.dp)
            ) {
                Text(text = "确定", fontSize = 14.sp)
            }
        },
        dismissButton = {
            Button(
                onClick = {
                }, modifier = Modifier
//                    .height(55.dp)
            ) {
                Text(text = "取消", fontSize = 14.sp)
            }
        },
        title = {
            Text(text = "添加分类", fontSize = 16.sp)
        },
        text = {
            TextField(
                value = valueState.value,
                onValueChange = { valueState.value = it },
                colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White),
            )
        })
}