package com.fanketly.module_glance

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.glance.GlanceModifier
import androidx.glance.appwidget.*
import androidx.glance.appwidget.lazy.LazyColumn
import androidx.glance.appwidget.lazy.items
import androidx.glance.layout.fillMaxSize
import androidx.glance.layout.fillMaxWidth
import androidx.glance.layout.padding
import androidx.glance.text.FontWeight
import androidx.glance.text.Text
import androidx.glance.text.TextStyle

/**
 *Create by Fanketly on 2022/7/12
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
class NoteGlanceWidget : GlanceAppWidget() {
    @Composable
    override fun Content() {
        val list = listOf("A", "B", "C", "D", "E")
        LazyColumn(
            modifier = GlanceModifier
                .fillMaxSize()
                .background(day = Color.White, night = Color.LightGray)
                .appWidgetBackground()
                .cornerRadius(16.dp)
                .padding(8.dp)
        ) {
            items(list) {
                Text(
                    text = it,
                    modifier = GlanceModifier.fillMaxWidth(),
                    style = TextStyle(fontWeight = FontWeight.Bold),
                )
            }
        }
    }
}

class NoteGlanceWidgetReceiver : GlanceAppWidgetReceiver() {

    override val glanceAppWidget: GlanceAppWidget = NoteGlanceWidget()
}
