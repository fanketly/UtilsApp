package com.fanketly.module_login

import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.alibaba.android.arouter.facade.annotation.Route
import com.fanketly.core_lib_compose_ui.PasswordTextField
import com.fanketly.core_lib_compose_ui.UserNameTextField
import com.fanketly.core_lib_compose_ui.theme.UtilsAppTheme
import com.fanketly.module_login.domain.LoginRequester
import fanketly.core.lib.base.domain.CommonEvent
import fanketly.core.lib.base.ui.BaseComposeMVIActivity
import fanketly.core.lib.router.module.home.HomeNav
import fanketly.core.lib.router.module.login.LoginRouter

/**
 *Create by Fanketly on 2022/9/15
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
@Route(path = LoginRouter.PATH_MAIN, group = LoginRouter.GROUP)
class LoginActivity : BaseComposeMVIActivity<LoginRequester>() {

    override fun initView() {
        val usernameState = mutableStateOf("")
        val passwordState = mutableStateOf("")
        setContent {
            UtilsAppTheme {
                ConstraintLayout(
                    modifier = Modifier
                        .fillMaxHeight()
                        .fillMaxWidth(),
                ) {
                    val (logo, username, password, btn) = createRefs()
                    Image(
                        modifier = Modifier
                            .size(240.dp)
                            .padding(top = 8.dp)
                            .constrainAs(logo) {
                                top.linkTo(parent.top)
                                start.linkTo(parent.start)
                                end.linkTo(parent.end)
                            },
                        painter = painterResource(id = R.drawable.fun_tools_logo),
                        contentDescription = "logo",
                    )
                    UserNameTextField(
                        textValue = usernameState,
                        modifier = Modifier.constrainAs(username) {
                            top.linkTo(logo.bottom)
                            start.linkTo(logo.start)
                            end.linkTo(logo.end)
                        })
                    PasswordTextField(
                        textValue = passwordState,
                        modifier = Modifier.constrainAs(password) {
                            top.linkTo(username.bottom, 12.dp)
                            start.linkTo(username.start)
                            end.linkTo(username.end)
                        }
                    )
                    Button(
                        onClick = {
                            viewModel.input(
                                CommonEvent.QueryAny(
                                    mapOf(
                                        Pair("username", usernameState.value),
                                        Pair("password", passwordState.value)
                                    )
                                )
                            )
                        }, modifier = Modifier
                            .height(55.dp)
                            .constrainAs(btn) {
                                width = Dimension.fillToConstraints
                                top.linkTo(password.bottom, 24.dp)
                                start.linkTo(password.start)
                                end.linkTo(password.end)
                            }
                    ) {
                        Text(text = stringResource(id = R.string.core_ui_login), fontSize = 20.sp)
                    }
                }
            }
        }
    }

    override fun initData() {
    }

    override fun onInput() {
    }

    override fun onOutput() {
        viewModel.output(this) {
            if (it is CommonEvent.QueryAny) {
                HomeNav.routerMain()
                finish()
            }
        }
    }

}