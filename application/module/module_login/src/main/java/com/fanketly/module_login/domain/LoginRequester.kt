package com.fanketly.module_login.domain

import fanketly.core.lib.base.domain.CommonEvent
import fanketly.core.lib.base.domain.MviDispatcherKTX

/**
 *Create by Fanketly on 2022/9/15
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
class LoginRequester : MviDispatcherKTX<CommonEvent>() {

    override suspend fun onHandle(event: CommonEvent) {
        if (event is CommonEvent.QueryAny) {
                sendResult(event)
        }
    }
}