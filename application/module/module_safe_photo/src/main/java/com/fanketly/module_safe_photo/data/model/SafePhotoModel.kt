package com.fanketly.module_safe_photo.data.model

import androidx.room.Entity
import androidx.room.Ignore

/**
 *Create by Fanketly on 2022/9/12
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
@Entity(tableName = "safe_photo_urls")
class SafePhotoModel() : BasePhotoModel() {

    @Ignore
    constructor(
        path: String,
        position: Long,
        sortPosition: Int
    ) : this() {
        this.path = path
        this.position = position
        this.sortPosition = sortPosition
    }

}