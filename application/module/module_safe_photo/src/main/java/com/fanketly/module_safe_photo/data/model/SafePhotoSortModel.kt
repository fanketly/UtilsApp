package com.fanketly.module_safe_photo.data.model

import androidx.room.Entity
import androidx.room.Ignore

/**
 *Create by Fanketly on 2022/9/19
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
@Entity(tableName = "safe_photo_sort")
class SafePhotoSortModel() : BasePhotoModel() {
    lateinit var sortName: String

    @Ignore
    constructor(
        sortName: String,
        path: String?,
        position: Long,
        sortPosition: Int
    ) : this() {
        this.path = path
        this.sortName = sortName
        this.position = position
        this.sortPosition = sortPosition
    }


}


