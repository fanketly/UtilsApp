package com.fanketly.module_safe_photo.domain

import com.fanketly.module_safe_photo.data.model.BasePhotoModel
import com.fanketly.module_safe_photo.data.model.SafePhotoModel
import com.fanketly.module_safe_photo.data.model.SafePhotoSortModel

sealed class SafePhotoEvent {
    data class GetPhotoList(
        var sortPosition: Int = 0,
        var list: List<SafePhotoModel>? = null
    ) : SafePhotoEvent()

    data class AddPhoto(var any: SafePhotoModel? = null) : SafePhotoEvent()
    data class DeletePhoto(
        var basePhotoModel: BasePhotoModel? = null,
        var isSuccess: Boolean = true
    ) : SafePhotoEvent()

    data class SwitchPhoto(
        var sourceModel: BasePhotoModel? = null,
        var isSuccess: Boolean = true
    ) : SafePhotoEvent()

    ///////////////////////////////////////////////////////////////////////////
    //
    ///////////////////////////////////////////////////////////////////////////

    data class GetPhotoSortList(var list: List<SafePhotoSortModel>? = null) : SafePhotoEvent()
    data class AddPhotoSort(var any: SafePhotoSortModel? = null) : SafePhotoEvent()

    data class DeletePhotoSort(var isSuccess: Boolean = true) : SafePhotoEvent()
    data class EditPhotoSort(
        val model: SafePhotoSortModel? = null, var isSuccess: Boolean = true
    ) : SafePhotoEvent()
}