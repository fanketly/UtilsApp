package com.fanketly.module_safe_photo.data.model

import androidx.room.PrimaryKey

/**
 *Create by Fanketly on 2022/9/20
 *Gitee:https://gitee.com/fanketly
 *desc:抽象类提高复用性
 */
abstract class BasePhotoModel {
    @JvmField
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    @JvmField
    var position: Long = 0

    @JvmField
    var sortPosition: Int = 0

    @JvmField
    var path: String? = null

}