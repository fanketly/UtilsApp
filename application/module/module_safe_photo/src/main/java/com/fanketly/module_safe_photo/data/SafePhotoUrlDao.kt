package com.fanketly.module_safe_photo.data

import androidx.room.*
import com.fanketly.module_safe_photo.data.model.SafePhotoModel

@Dao
interface SafePhotoUrlDao {

    @Query("select * from safe_photo_urls where sortPosition=:sortPosition order by position")
    fun getBySortPosition(sortPosition: Int): List<SafePhotoModel>

    @Insert
    fun insert(safeUrlModel: SafePhotoModel): Long

    @Update
    fun update(safeUrlModel: SafePhotoModel): Int

    @Delete
    fun delete(safeUrlModel: SafePhotoModel): Int
}