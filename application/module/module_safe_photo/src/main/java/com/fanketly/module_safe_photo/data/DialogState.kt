package com.fanketly.module_safe_photo.data

import com.fanketly.module_safe_photo.data.model.SafePhotoSortModel

sealed class DialogState {
    class OpenEdit(val model: SafePhotoSortModel,val index:Int) : DialogState()
    object Open : DialogState()

}