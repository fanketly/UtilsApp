package com.fanketly.module_safe_photo.ui

import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.blankj.utilcode.util.LogUtils
import com.fanketly.core_lib_compose_ui.theme.Alpha70Grey
import com.fanketly.module_safe_photo.R
import com.fanketly.module_safe_photo.data.model.BasePhotoModel
import com.fanketly.module_safe_photo.data.model.SafePhotoSortModel
import org.burnoutcrew.reorderable.*

/**
 *Create by Fanketly on 2022/9/8
 *Gitee:https://gitee.com/fanketly
 *desc:
 */


@OptIn(ExperimentalFoundationApi::class)
@Composable
fun <E : BasePhotoModel> VerticalReorderGrid(
    count: Int,
//    data: MutableState<List<E>>,
    data: List<E>,
    onMove: (ItemPosition, ItemPosition) -> Unit,
    gridModifier: Modifier = Modifier,
    photoModifier: Modifier = Modifier,
    onDoubleClick: ((model: E, index: Int) -> Unit)? = null,
    onClick: (model: E, index: Int) -> Unit
) {
    val state = rememberReorderableLazyGridState(
//            dragCancelledAnimation = NoDragCancelledAnimation(),
        onMove = onMove, canDragOver = { true })

    LazyVerticalGrid(
        columns = GridCells.Fixed(count),
        state = state.gridState,
        contentPadding = PaddingValues(horizontal = 8.dp),
        verticalArrangement = Arrangement.spacedBy(4.dp),
        horizontalArrangement = Arrangement.spacedBy(4.dp),
        modifier = gridModifier
            .reorderable(state)
            .detectReorderAfterLongPress(state)
    ) {
        LogUtils.d(data.size)
//        提供一个稳定的键可以使 item 状态在数据集变化时保持一致
        itemsIndexed(data, key = { _, item -> item.position }) { index, item ->
//        }
//        items(data, key = { it.position }) { item ->
            ReorderableItem(
                state, key = item.position, defaultDraggingModifier = Modifier.padding(4.dp)
            ) { isDragging ->
                val elevation = animateDpAsState(if (isDragging) 16.dp else 0.dp)
                Photo(
                    url = item.path,
                    content = if (item is SafePhotoSortModel) item.sortName else null,
                    elevation = elevation,
                    modifier = photoModifier.combinedClickable(onDoubleClick = {
                        onDoubleClick?.invoke(item, index)
                    }, onClick = {
                        onClick.invoke(item, index)
                    })
                )

            }
        }
    }

}




@Composable
fun Photo(url: Any?, content: String?, elevation: State<Dp>, modifier: Modifier) {
    Box(
        modifier = modifier
            .shadow(elevation.value)
            .aspectRatio(1f)
            .size(80.dp, 100.dp)
    ) {
        AsyncImage(
            model = url ?: R.drawable.core_ui_not_photo,
            contentDescription = null,
            modifier = Modifier.fillMaxSize()
        )
        if (content != null) Text(
            text = content,
            textAlign = TextAlign.Center,
            color = Color.Black,
            fontWeight = FontWeight.Bold,
            modifier = Modifier
                .fillMaxWidth()
                .background(Alpha70Grey)
                .align(Alignment.BottomCenter)
        )
    }
}

