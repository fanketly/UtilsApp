package com.fanketly.module_safe_photo.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.fanketly.module_safe_photo.data.model.SafePhotoModel
import com.fanketly.module_safe_photo.data.model.SafePhotoSortModel

/**
 *Create by Fanketly on 2022/7/16
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
@Database(entities = [SafePhotoModel::class,SafePhotoSortModel::class], version = 1)
abstract class SafeDatabase : RoomDatabase() {
    abstract fun safeUrlDao(): SafePhotoUrlDao
    abstract fun safePhotoSortDao(): SafePhotoSortDao
}