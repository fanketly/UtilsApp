package com.fanketly.module_safe_photo.data

import androidx.room.*
import com.fanketly.module_safe_photo.data.model.SafePhotoModel
import com.fanketly.module_safe_photo.data.model.SafePhotoSortModel

/**
 *Create by Fanketly on 2022/7/16
 * Modified 2022/8/18
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
@Dao
interface SafePhotoSortDao {

    @Query("select * from safe_photo_sort order by position")
    fun getAll(): List<SafePhotoSortModel>

    @Insert
    fun insert(safeUrlModel: SafePhotoSortModel): Long

    @Update
    fun update(safeUrlModel: SafePhotoSortModel): Int

    @Delete
    fun delete(safeUrlModel: SafePhotoSortModel): Int
}