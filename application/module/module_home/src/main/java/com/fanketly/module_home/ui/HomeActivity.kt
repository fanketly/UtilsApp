package com.fanketly.module_home.ui

import androidx.activity.compose.setContent
import com.alibaba.android.arouter.facade.annotation.Route
import com.fanketly.core_lib_compose_ui.data.DrawerItem
import com.fanketly.core_lib_compose_ui.theme.UtilsAppTheme
import com.fanketly.module_home.R
import com.fanketly.module_home.domain.HomeRequester
import fanketly.core.lib.base.ui.BaseComposeMVIActivity
import fanketly.core.lib.router.module.home.HomeRouter
import fanketly.core.lib.router.module.safe.SafeNav
import fanketly.core.lib.router.module.safe_photo.SafePhotoNav

/**
 *Create by Fanketly on 2022/9/18
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
@Route(path = HomeRouter.PATH_MAIN, group = HomeRouter.GROUP)
class HomeActivity : BaseComposeMVIActivity<HomeRequester>() {

    override fun initView() {
        val homeList = listOf(
            DrawerItem(R.drawable.fun_tools_logo, textId = R.string.core_ui_safe_url),
            DrawerItem(R.drawable.fun_tools_logo, textId = R.string.core_ui_safe_photo)
        )
        setContent {
            UtilsAppTheme {
                AppDrawer(homeList) {
                    moduleRouter(it)
                }
            }
        }
    }

    override fun initData() {
    }

    override fun onInput() {
    }

    override fun onOutput() {
    }

    private fun moduleRouter(drawerItem: DrawerItem) {
        when (drawerItem.textId) {
            R.string.core_ui_safe_url -> {
                SafeNav.routerMain()
            }
            R.string.core_ui_safe_photo -> {
                SafePhotoNav.routerMain()
            }
        }
    }
}