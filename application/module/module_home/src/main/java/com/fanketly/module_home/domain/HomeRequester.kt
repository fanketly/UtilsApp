package com.fanketly.module_home.domain

import com.fanketly.core_lib_compose_ui.data.DrawerItem
import com.fanketly.module_home.R
import fanketly.core.lib.base.domain.CommonEvent
import fanketly.core.lib.base.domain.MviDispatcherKTX

/**
 *Create by Fanketly on 2022/9/15
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
class HomeRequester : MviDispatcherKTX<CommonEvent>() {

    override suspend fun onHandle(event: CommonEvent) {

    }
}