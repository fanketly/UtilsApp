package com.fanketly.module_home.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.fanketly.core_lib_compose_ui.IconRowItem
import com.fanketly.core_lib_compose_ui.MyBottomAppBar
import com.fanketly.core_lib_compose_ui.MyTopAppBar
import com.fanketly.core_lib_compose_ui.data.DrawerItem
import com.fanketly.module_home.R
import kotlinx.coroutines.CoroutineScope

/**
 *Create by Fanketly on 2022/9/18
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
@Composable
fun AppDrawer(list: List<DrawerItem>, click: (DrawerItem) -> Unit) {
    val scaffoldState: ScaffoldState = rememberScaffoldState()
    val scope: CoroutineScope = rememberCoroutineScope()
    Scaffold(
        scaffoldState = scaffoldState,
        contentColor = MaterialTheme.colors.primary,
        content = { },
        topBar = {
            MyTopAppBar(
                scaffoldState = scaffoldState,
                scope = scope
            )
        },
        bottomBar = { MyBottomAppBar() },
        floatingActionButton = {},
        floatingActionButtonPosition = FabPosition.End,
        snackbarHost = { SnackbarHost(it) },
        drawerContent = {
            Icon(
                painter = painterResource(id = R.drawable.fun_tools_logo),
                contentDescription = "logo",
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(0.4f)
            )
            DrawerItems(list, click)
        },
        drawerGesturesEnabled = true,
        drawerShape = MaterialTheme.shapes.large,
        drawerElevation = DrawerDefaults.Elevation,
        drawerBackgroundColor = MaterialTheme.colors.surface,
//    drawerContentColor = contentColorFor(drawerBackgroundColor),
        drawerScrimColor = DrawerDefaults.scrimColor,
        backgroundColor = MaterialTheme.colors.background,
    )
}

@Composable
fun DrawerItems(list: List<DrawerItem>, click: (DrawerItem) -> Unit) {
    Column {
        list.forEach {
            IconRowItem(
                painter = painterResource(id = it.painterId),
                contentDescription = it.contentDescription,
                textId = it.textId,
                rowModifier = Modifier.clickable {
                    click.invoke(it)
                },
                iconModifier = Modifier.size(48.dp),
                textModifier = Modifier.padding(start = 4.dp)
            )
        }
    }
}


