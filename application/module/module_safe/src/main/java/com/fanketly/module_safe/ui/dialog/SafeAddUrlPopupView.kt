package com.fanketly.module_safe.ui.dialog

import android.annotation.SuppressLint
import android.content.Context
import com.blankj.utilcode.util.RegexUtils
import com.blankj.utilcode.util.ToastUtils
import com.fanketly.module_safe.R
import com.fanketly.module_safe.databinding.DialogAddItemBinding
import com.lxj.xpopup.core.CenterPopupView

/**
 *Create by Fanketly on 2022/8/14
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
@SuppressLint("ViewConstructor")
class SafeAddUrlPopupView(
    context: Context,
    private val url: String?,
    private val name: String?,
    private val onConfirm: (url: String, name: String) -> Unit
) : CenterPopupView(context) {

    override fun getImplLayoutId(): Int {
        return R.layout.dialog_add_item
    }

    override fun onCreate() {
        super.onCreate()
        val bind = DialogAddItemBinding.bind(popupImplView)
        if (url != null) bind.etDialogAddUrl.setText(url)
        if (name != null) bind.etDialogAddName.setText(name)
        bind.btnDialogCancel.setOnClickListener {
            dismiss()
        }
        bind.btnDialogConfirm.setOnClickListener {
            val url = bind.etDialogAddUrl.text.toString()
            val name = bind.etDialogAddName.text.toString()
            if (url.isEmpty() || name.isEmpty()) {
                ToastUtils.showShort("输入框不能为空")
            } else {
                if (!RegexUtils.isURL(url)) {
                    ToastUtils.showShort("地址格式错误，请重新确认")
                    return@setOnClickListener
                }
                onConfirm.invoke(url, name)
                dismiss()
            }
        }
    }
}