package com.fanketly.module_safe.data

import androidx.room.*
import com.fanketly.module_safe.data.model.SafeUrlModel

/**
 *Create by Fanketly on 2022/7/16
 * Modified 2022/8/18
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
@Dao
interface SafeUrlDao {

    @Query("select * from safe_urls order by position")
    fun getAll(): List<SafeUrlModel>

    @Insert
    fun insert(safeUrlModel: SafeUrlModel):Long

    @Update
    fun update(safeUrlModel: SafeUrlModel): Int

    @Delete
    fun delete(safeUrlModel: SafeUrlModel): Int
}