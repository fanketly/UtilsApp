//package com.jetusesoft.wmhouseapp_erp
//
//import android.annotation.SuppressLint
//import android.content.Context
//import android.content.Intent
//import android.graphics.Color
//import android.os.Bundle
//import android.view.View
//import android.view.ViewGroup
//import android.widget.Button
//import android.widget.EditText
//import android.widget.TextView
//import androidx.appcompat.app.AppCompatActivity
//import androidx.core.widget.doOnTextChanged
//import androidx.lifecycle.LifecycleOwner
//import androidx.recyclerview.widget.RecyclerView
//import com.blankj.utilcode.util.TimeUtils
//import com.drake.brv.BindingAdapter
//import com.fanketly.safe.R
//import com.fanketly.safe.databinding.ActivitySafeBinding
//import fanketly.core.lib.base.app.BaseViewBindingActivity
//
//import kotlinx.coroutines.CoroutineScope
//import kotlinx.coroutines.Dispatchers
//import kotlinx.coroutines.launch
//import kotlinx.coroutines.withContext
//import java.text.ParseException
//import java.text.SimpleDateFormat
//import java.util.*
//
///**
// * CREATE BY 黄炜强
// * 2022/7/25 09:12
// * Desc:
// * */
//class TryAny : BaseViewBindingActivity<ActivitySafeBinding>() {
//    override fun baseLayoutId(): Int {
//        return R.layout.activity_safe
//    }
//
//    override fun baseLayoutView(): View? {
//        return null
//    }
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//
//    }
//}
//
///**
// * 想法：无需关心代码实现方式
// * 只调用方法自动实现
// *
// * */
//class VM<E>(val repo: EasyHttpRepoKt) : EasyHttpViewModelKt<E>() {
//}
//
//inline fun <E> AutoFactory<E>.launch(block: AutoFactory<E>.() -> Unit) {
//
//}
//
//class AutoFactory<E>(
//    repo: EasyHttpRepoKt = EasyHttpRepoKt(),
////    block: (VM<E>) -> AutoFactory<E>
//) {
//    val edtMap = mutableMapOf<Int, TextView>()
//    val vm: VM<E> by lazy { VM(repo) }
//
//    init {
////        block.invoke(VM<E>(repo))
//    }
//
//    class Http(block: Http.() -> Unit) : Auto() {
//
//
//    }
//
//
//    class DataBase : Auto() {
//
//    }
//
//
//    fun Auto.data() {
//
//    }
//
//    class Rv<E>(
//        val recyclerView: RecyclerView,
//        val list: MutableList<E>,
//        block: Rv<E>.(RecyclerView) -> Unit,
//    ) : BindingAdapter() {
//        //        lateinit var adapter: Rv<E>
//        fun setup(block: Rv<E>.(RecyclerView) -> Unit): Rv<E> {
//            val adapter = Rv(recyclerView, list, block)
//            adapter.block(recyclerView)
//            recyclerView.adapter = adapter
//            return adapter
//        }
//
//        init {
//
//            setup(block).models = list
//        }
//
//        fun E.add() {
//            list.add(this)
//            recyclerView.adapter?.notifyItemInserted(list.size - 1)
//        }
//
//        fun E.update(position: Int) {
//            list.removeAt(position)
//            list.add(position, this)
//            recyclerView.adapter?.notifyItemChanged(position)
//        }
//
//        fun List<E>.addAll() {
//            val size = list.size
//            list.addAll(this)
//            recyclerView.adapter?.notifyItemRangeInserted(size, this.size)
//        }
//
//        fun List<E>.reset() {
//            list.clear()
//            list.addAll(this)
//            recyclerView.adapter?.notifyDataSetChanged()
//        }
//    }
//
//    fun Button.resetQueryText() {
//        edtMap.values.forEach {
//            it.setText("")
//        }
//    }
//
//
//    fun asQueryEdt(vararg editText: TextView) {
//        editText.forEach { edtMap[it.id] = it }
//    }
//
//    fun EditText.listener(
//        type: TextType = TextType.STRING,
//        action: (
//            isMatch: Boolean,
//            text: CharSequence?
//        ) -> Unit
//    ) {
//
//        if (type == TextType.STRING) this.doOnTextChanged { text, _, _, _ ->
//            action.invoke(
//                true,
//                text
//            )
//        }
//        else this.addTextChangedListener(object : NumberTextWatcher(type) {
//            override fun textChanged(
//                s: CharSequence?,
//                start: Int,
//                before: Int,
//                count: Int,
//                isMatch: Boolean
//            ) {
//                action.invoke(isMatch, s)
//            }
//        })
//    }
//
//    fun EditText.match() {
//
//    }
//}
//
//fun TextView.showDatePickClick(
//    activity: AppCompatActivity,
//    date: ((String) -> Unit)? = null
//) {
//    this.setOnClickListener {
//        showDatePickDialog(activity, this, date)
//    }
//}
//
//@Throws(ParseException::class)
//private fun showDatePickDialog(
//    activity: AppCompatActivity,
//    tv: TextView,
//    date: ((String) -> Unit)?
//) {
//    CustomDatePickerDialogFragment.newInstance(tv.text.toString())
//        .selectDate { date1: String?, _: Int, _: Int, _: Int ->
//            date?.invoke(date1!!)
//            tv.text = date1
//        }.show(activity)
//
//}
//
//abstract class Auto {
//
//}