package com.fanketly.module_safe.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.fanketly.module_safe.data.model.SafeUrlModel

/**
 *Create by Fanketly on 2022/7/16
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
@Database(entities = [SafeUrlModel::class], version = 1)
abstract class SafeDatabase : RoomDatabase() {
    abstract fun safeUrlDao(): SafeUrlDao
}