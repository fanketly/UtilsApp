package com.fanketly.module_safe.domain

import com.fanketly.module_safe.data.model.SafeUrlModel

/**
 *Create by Fanketly on 2022/8/18
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
sealed class UrlMemoEvent {
    var sourceModel: SafeUrlModel? = null

    data class GetMemoList(var memoList: List<SafeUrlModel>? = null) : UrlMemoEvent()
    data class AddMemo(var memo: SafeUrlModel? = null) : UrlMemoEvent()
    data class SwitchMemo(var isSuccess: Boolean = true) : UrlMemoEvent()
    data class DeleteMemo(var isSuccess: Boolean = true) : UrlMemoEvent()
    data class EditMemo(var isSuccess: Boolean = true) : UrlMemoEvent()
}