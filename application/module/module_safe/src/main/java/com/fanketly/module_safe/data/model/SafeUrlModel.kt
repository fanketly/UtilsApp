package com.fanketly.module_safe.data.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.drake.brv.annotaion.ItemOrientation
import com.drake.brv.item.ItemDrag

/**
 *Create by Fanketly on 2022/7/1
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
@Entity(tableName = "safe_urls")
data class SafeUrlModel(
    @PrimaryKey(autoGenerate = true) var id: Long,
    var url: String?,
    var name: String?,
    var position: Long,
    val time: String? = null,
    @Ignore override var itemOrientationDrag: Int
) : ItemDrag {

    constructor(
        url: String?,
        name: String?,
        position: Long,
        time: String? = null
    ) : this(0, url, name, position, time, ItemOrientation.VERTICAL)
}
