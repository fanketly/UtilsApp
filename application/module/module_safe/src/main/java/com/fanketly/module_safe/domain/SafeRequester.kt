package com.fanketly.module_safe.domain

import com.blankj.utilcode.util.LogUtils
import com.fanketly.module_safe.data.repo.SafeRepo
import fanketly.core.lib.base.domain.MviDispatcherKTX

/**
 *Create by Fanketly on 2022/7/3
 * Modified 2022/8/18
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
class SafeRequester : MviDispatcherKTX<UrlMemoEvent>() {
    private val safeRepo by lazy { SafeRepo() }
    override suspend fun onHandle(event: UrlMemoEvent) {
        when (event) {
            is UrlMemoEvent.AddMemo -> {
                val memo = event.memo!!
                memo.id = safeRepo.add(memo)
                LogUtils.d(memo.position)
                sendResult(event)
            }
            is UrlMemoEvent.GetMemoList -> sendResult(event.copy(safeRepo.getAll()))
            is UrlMemoEvent.SwitchMemo -> {
                if (safeRepo.update(event.sourceModel!!) >= 0) {
                    sendResult(event.copy())
                } else {
                    sendResult(event.copy(false))
                }
            }
            is UrlMemoEvent.EditMemo -> {
                if (safeRepo.update(event.sourceModel!!) >= 0) {
                    sendResult(event.copy())
                } else {
                    sendResult(event.copy(false))
                }
            }
            is UrlMemoEvent.DeleteMemo -> {
                if (safeRepo.delete(event.sourceModel!!) >= 0) {
                    sendResult(event.copy())
                } else {
                    sendResult(event.copy(false))
                }
            }
        }
    }


}