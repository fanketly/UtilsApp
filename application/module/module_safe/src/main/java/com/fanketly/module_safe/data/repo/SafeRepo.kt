package com.fanketly.module_safe.data.repo

import androidx.room.Room
import com.blankj.utilcode.util.LogUtils
import com.fanketly.module_safe.data.SafeDatabase
import com.fanketly.module_safe.data.model.SafeUrlModel
import fanketly.core.CoreApp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 *Create by Fanketly on 2022/7/15
 * Modified 2022/8/18
 *Gitee:https://gitee.com/fanketly
 *desc:
 */
class SafeRepo {
    private val safeUrlDao by lazy {
        val build = Room.databaseBuilder(CoreApp.context, SafeDatabase::class.java, "safe_db")
            .enableMultiInstanceInvalidation().build()
        LogUtils.d(build)
        build.safeUrlDao()
    }

    suspend fun getAll(): List<SafeUrlModel> = withContext(Dispatchers.IO) { safeUrlDao.getAll() }

    suspend fun add(safeUrlModel: SafeUrlModel) =
        withContext(Dispatchers.IO) { safeUrlDao.insert(safeUrlModel) }

    suspend fun update(safeUrlModel: SafeUrlModel): Int =
        withContext(Dispatchers.IO) {
            safeUrlDao.update(safeUrlModel)
        }

    suspend fun delete(safeUrlModel: SafeUrlModel): Int =
        withContext(Dispatchers.IO) {
            safeUrlDao.delete(safeUrlModel)
        }
}